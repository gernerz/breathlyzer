//---------------------------------------------------------------------
// Group:   ger_mar_sch_spe
// Project: SE4130 Lab5: Blood Alcohol Content Monitor
// Purpose: This class drives a 2 line, 16 column LCD display using a pic16
//---------------------------------------------------------------------
#ifndef __LCD_H
#define __LCD_H

#include "Globals.h"

//-----------------------------------------------------------------------------
// The method initializes the LCD. For this particular lab, we will be using
// the LCD in 4 bit data entry mode. For displaying, we will be using 2 lines 
// to display the data. The data will be incremented from right to left, using 
// a blinking cursor, and will be using a 5x7 font.
//-----------------------------------------------------------------------------
void initLCD(void);

//-----------------------------------------------------------------------------
// Calls the clear display command. The 1.64mS delay is included in this
// function to give the LCD adequate time to execute the command.
//-----------------------------------------------------------------------------
void clearDisplay(void);

//-----------------------------------------------------------------------------
// Sends data to the LCD GCRAM. The high nibble is sent first, low nibble sent
// last. The commands are masked and then shifted in order to match up with
// 4 LCD data lines that are connected to port B.
//-----------------------------------------------------------------------------
void sendData(BYTE data);

//-----------------------------------------------------------------------------
// Send a character string the LCD. The loop will continue until a null
// terminator is reached.
//-----------------------------------------------------------------------------
void sendString(const char* s);

//-----------------------------------------------------------------------------
// Sets the cursor to the given row and column. The LCD is zero indexed, so row
// one is represented by 0 and row two is represented by 1. The high nibble is
// masked off to ensure the user does not send unwanted commands to the LCD by
// accident.
//-----------------------------------------------------------------------------
void setCursor(char row, char col);

#endif