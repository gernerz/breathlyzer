 //---------------------------------------------------------------------
// Group:   ger_mar_sch_spe
// Project: SE4130 Lab5: Blood Alcohol Content Monitor
// Purpose: This module is designed to drive the SPI interface on a PIC16.
//          Specialized methods are included to drive a digital potentiometer. 
//---------------------------------------------------------------------
#ifndef __SPI_H
#define __SPI_H
#include <pic.h>
#include "Globals.h"

// -------------------------------------------------------------------
// Send and recieve data accross the SPI interface
// -------------------------------------------------------------------
void sendSPIData(BYTE buffer);

// -------------------------------------------------------------------
// Sets the resistance to the passed value. Output range: [0.1k,99k]
// -------------------------------------------------------------------
void setResistance(int res);

#endif