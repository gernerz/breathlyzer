//---------------------------------------------------------------------
// Group:   ger_mar_sch_spe
// Project: SE4130 Lab5: Blood Alcohol Content Monitor
// Purpose: This file implements the methods defines in LCD.h. These methods
//          are designed to drive a 2 line 16 column LCD display with a pic16
//---------------------------------------------------------------------
#include "LCD.h"

//-----------------------------------------------------------------------------
// Setting up the port defines
//-----------------------------------------------------------------------------
#define ENABLE_BIT                  RC7   // Port connect to LCD enable pin
#define LCD_DATA_LINE               PORTD // Port connect to the LCD data lines
#define REGISTER_SELECT             RC6   // Port connects the the LCD RS pin

//-----------------------------------------------------------------------------
// LCD command defines
//-----------------------------------------------------------------------------
#define CLEAR_DISPLAY               0x01  // LCD command to clear the display
#define CURSOR_SET_COMMAND_BIT      0x80  // Mask to know that we are setting the cursor location
#define DISPLAY_OFF                 0x08  // Turns display off
#define DISPLAY_ON_BLINKING         0x0D  // Turns display on and makes it blink
#define ENTRY_MODE_INC_RIGHT        0x06  // Sets entry mode to shift right
#define FOUR_BIT_DATA_MODE          0x20  // Set the LCD to 4-bit data entry mode
#define FUNCTION_SET_4BIT_2LINES    0x28  // Sets the LCD to 4-bit, 2-line mode, and 5x7 font
#define GET_LCD_ATTENTION           0x30  // Pre-shifted command for getting LCDs attention
#define ROW_TWO_DDRAM_ADDRESS       0x40  // Address of row two, column one

//-----------------------------------------------------------------------------
// Delay values
//-----------------------------------------------------------------------------
#define CLEAR_DISPLAY_DELAY         225  // Gives us a 6ms delay
#define FIVE_MS_DELAY_AMOUNT        350   // Gives us a 11ms delay
#define HUNDRED_US_DELAY_AMOUNT     200   // Gives us a 5ms delay
#define SEND_BYTE_DELAY_AMOUNT      125    // Gives us a 2ms delay
#define TWO_MS_DELAY_AMOUNT         125    // Gives us a 2ms delay
#define VCC_RISE_DELAY_AMOUNT       19350 // Gives us a 750ms delay

//-----------------------------------------------------------------------------
// Masks and shifts
//-----------------------------------------------------------------------------
#define HIGH_NIBBLE_MASK            0xF0  // Masks off the low nibble
#define LOW_NIBBLE_MASK             0x0F  // Masks off the high nibble
#define LOW_NIBBLE_SHIFT            0x04  // Shifts the low nibble to the correct data bits

//-----------------------------------------------------------------------------
// Misc. Defines
//-----------------------------------------------------------------------------
#define FIRST_COLUMN                0  // Sets the cursor to the first column
#define SECOND_ROW                  1  // Sets the cursor to the second row
#define WRITE_CONTROL_BITS          0  // Set RS to send command to DDRAM
#define WRITE_DATA_BITS             1  // Set RS to write data into CGRAM


static void LCDdelay(long delayAmount);
static void enableCycle(void);
static void sendCommand(BYTE command);

//-----------------------------------------------------------------------------
// The method initializes the LCD. For this particular lab, we will be using
// the LCD in 4 bit data entry mode. For displaying, we will be using 2 lines 
// to display the data. The data will be incremented from right to left, using 
// a blinking cursor, and will be using a 5x7 font. 
//-----------------------------------------------------------------------------
void initLCD(void)
{
   // -----------------------------------
   // Get the LCDs attention
   // -----------------------------------
   LCDdelay(VCC_RISE_DELAY_AMOUNT);
   REGISTER_SELECT = WRITE_CONTROL_BITS;
   LCD_DATA_LINE = GET_LCD_ATTENTION;
   enableCycle();
   LCDdelay(FIVE_MS_DELAY_AMOUNT); 
   enableCycle();
   LCDdelay(HUNDRED_US_DELAY_AMOUNT); 
   enableCycle();
   // -----------------------------------
   // End getting the LCDs attention
   // -----------------------------------
   LCDdelay(TWO_MS_DELAY_AMOUNT);
   LCD_DATA_LINE = FOUR_BIT_DATA_MODE; 
   enableCycle();
   sendCommand(FUNCTION_SET_4BIT_2LINES);
   LCDdelay(TWO_MS_DELAY_AMOUNT);
   sendCommand(DISPLAY_OFF);
   LCDdelay(TWO_MS_DELAY_AMOUNT);
   clearDisplay();
   sendCommand(DISPLAY_ON_BLINKING);
   LCDdelay(TWO_MS_DELAY_AMOUNT);
   sendCommand(ENTRY_MODE_INC_RIGHT);
   LCDdelay(TWO_MS_DELAY_AMOUNT);
   
}

//-----------------------------------------------------------------------------
// Sends a command to the LCD. The high nibble is sent first, low nibble sent
// last. The commands are masked and then shifted in order to match up with
// 4 LCD data lines that are connected to port D.
//-----------------------------------------------------------------------------
static void sendCommand(BYTE command)
{
   REGISTER_SELECT = WRITE_CONTROL_BITS;
   LCD_DATA_LINE = (command & HIGH_NIBBLE_MASK);
   enableCycle();
   LCDdelay(SEND_BYTE_DELAY_AMOUNT); 
   LCD_DATA_LINE = ((command & LOW_NIBBLE_MASK) << LOW_NIBBLE_SHIFT);
   enableCycle();
   LCDdelay(SEND_BYTE_DELAY_AMOUNT); 
}   

//-----------------------------------------------------------------------------
// Sends data to the LCD CGRAM. The high nibble is sent first, low nibble sent
// last. The commands are masked and then shifted in order to match up with
// 4 LCD data lines that are connected to port D.
//-----------------------------------------------------------------------------
void sendData(BYTE data)
{
   REGISTER_SELECT = WRITE_DATA_BITS;
   LCD_DATA_LINE = (data & HIGH_NIBBLE_MASK);
   enableCycle();
   LCDdelay(SEND_BYTE_DELAY_AMOUNT); 
   LCD_DATA_LINE = ((data & LOW_NIBBLE_MASK) << LOW_NIBBLE_SHIFT);
   enableCycle();
   LCDdelay(SEND_BYTE_DELAY_AMOUNT); 
}

//-----------------------------------------------------------------------------
// Cycles the LCD enable signal in order to send the command or data to the
// LCD. There is a short delay in order to give sufficient time for the data or
// command to send.
//-----------------------------------------------------------------------------
static void enableCycle(void)
{
   ENABLE_BIT = 1;
   asm("nop");       //Give enough time for the en
   asm("nop");
   asm("nop");
   asm("nop");
   ENABLE_BIT = 0;
}   

//-----------------------------------------------------------------------------
// Calls the clear display command. The delay is included in this function to
// give the LCD adequate time to execute the command.
//-----------------------------------------------------------------------------
void clearDisplay(void)
{
   sendCommand(CLEAR_DISPLAY);
   LCDdelay(CLEAR_DISPLAY_DELAY);
}   

//-----------------------------------------------------------------------------
// Send a character string the LCD. The loop will continue until a null
// terminator is reached.
//-----------------------------------------------------------------------------
void sendString(const char* string)
{
   while(*string)
      sendData(*string++);
}   

//-----------------------------------------------------------------------------
// Sets the cursor to the given row and column. The LCD is zero indexed, so row
// one is represented by 0 and row two is represented by 1.
//-----------------------------------------------------------------------------
void setCursor(char row, char col)
{
   if (row == 0)
      sendCommand(col | CURSOR_SET_COMMAND_BIT);
   else if ( row == 1 )
      sendCommand((col + ROW_TWO_DDRAM_ADDRESS)| CURSOR_SET_COMMAND_BIT);
}

//-----------------------------------------------------------------------------
// Delays for a given length of time.
//-----------------------------------------------------------------------------
static void LCDdelay(long delayAmount)
{
   long i;
   for ( i = 0; i < delayAmount; i++)
      asm("nop");
}   

#ifdef __TESTING_LCD

#define FIVE_SEC_DELAY_AMOUNT       0xFFFF // Gives us a little over a 5s delay
#define ONE_SEC_DELAY_AMOUNT        0x3FFF // Gives us a little over a 1s delay

int main(void)
{
   TRISA = 0x00;
   TRISB = 0x3F;
   TRISC = 0x00;
   TRISD = 0x00;
   initLCD();
   clearDisplay();
   while(1)
   {
      sendString("Test curs set");
      LCDdelay(FIVE_SEC_DELAY_AMOUNT);
      setCursor(0x01,0x00);
      sendString("and wrap");
      LCDdelay(FIVE_SEC_DELAY_AMOUNT); //Five Second Delay for testing
      sendString(". Tst"); //Testing append
      LCDdelay(FIVE_SEC_DELAY_AMOUNT);
      setCursor(0x00,0x00);
      sendString("Test overwrite..");
      LCDdelay(FIVE_SEC_DELAY_AMOUNT);
      clearDisplay();
      sendString("Testing");
      setCursor(0x01,0x05); //Second row, 10th column
      sendString("cursor set."); //The period should be in column 20
      LCDdelay(FIVE_SEC_DELAY_AMOUNT);
      clearDisplay();
      setCursor(0x00,0x09); //First row, 13th column
      sendString("Testing");
      setCursor(0x01,0x00);
      sendString("curs set again.");
      LCDdelay(FIVE_SEC_DELAY_AMOUNT);
      clearDisplay();
      //--------------------------------------------------------
      // Beginning testing setCursor for every location on the LCD.
      //--------------------------------------------------------
      short row = 0, col = 0, i;
      for (i = 0; i < 16; i++)
      {
         setCursor(row,col + i);
         LCDdelay(ONE_SEC_DELAY_AMOUNT);
      }
      row++;
      for (i = 0; i < 16; i++)
      {
         setCursor(row,col + i);
         LCDdelay(ONE_SEC_DELAY_AMOUNT);
      }
      clearDisplay();
   }   
} 

#endif