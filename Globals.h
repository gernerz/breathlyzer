//---------------------------------------------------------------------
// Group:   ger_mar_sch_spe
// Project: SE4130 Lab5: Blood Alcohol Content Monitor
// Purpose: This file contains all global definitions
//---------------------------------------------------------------------
#ifndef __GLOBAL_H
#define __GLOBAL_H

#include <pic.h>

typedef unsigned char BYTE;
typedef signed char BOOL;
#define TRUE         1
#define FALSE        0

// -------------------------------------------------------------------
// Test Controls (only one of these can be uncommented)
// -------------------------------------------------------------------
#define __MAIN
//#define __TESTING_ADC
//#define __TESTING_LCD
//#define __TESTING_PWM
//#define __TESTING_SPI

#endif