
//---------------------------------------------------------------------
// Group:   ger_mar_sch_spe
// Project: SE4130 Lab5: Blood Alcohol Content Monitor
// Purpose: Main file driving the Blood Alcohol Content Monitor
//---------------------------------------------------------------------

//-----------------------------------------------------------------------------
//Includes
//-----------------------------------------------------------------------------
#include "Globals.h"
#include "LCD.h"
#include "ADC.h"
#include "SPI.h"
#include "PWM.h"
#include <string.h>
#include <stdio.h>
#include <pic.h>

// Osc = XT, Watchdog off, everything else off or disabled
__CONFIG(0x3F39);

#ifdef __MAIN

//-----------------------------------------------------------------------------
// Port setting defines
//-----------------------------------------------------------------------------
#define ADCON1_MASK                    0x84
#define INTCON_MASK                    0x00 //Do we need this?
#define OPTION_REG_MASK                0x00 
#define SSPCON_MASK                    0x30
#define SSPSTAT_MASK                   0x40
#define TRISA_MASK                     0x0F //I have no idea how this needs to be set
#define TRISB_MASK                     0x3F 
#define TRISC_MASK                     0x17 
#define TRISD_MASK                     0x00 //High nibble needs to be 0
#define T2CON_MASK                     0x05
#define CCP1CON_MASK                   0x0C

//-----------------------------------------------------------------------------
// Delay defines
//-----------------------------------------------------------------------------
#define QUARTER_SEC_DELAY              0x396A
#define FLASH_TIME                     0x196A

//-----------------------------------------------------------------------------
// Constant defines
//-----------------------------------------------------------------------------
#define TEMP_CHANNEL                   0x0
#define TEMP_ALPHA                     0.20
#define TEMP_ADJUST_SLOPE              -0.01
#define TEMP_ADJUST_INTERCEPT          1.2
#define CTOF_MULT                      1.8
#define CTOF_CONST                     32
#define GAS_CHANNEL                    0x3
#define GAS_ALPHA                      0.75
#define ADC_RES                        1023
#define CELSIUS_VAL                    500
#define LINE_LENGTH                    16
#define TEMP_MULT                      1000
#define MAX_BAC_VAL                    100
#define CAUTION                        60
#define DONT_DRIVE                     75
#define LOOKUP_TABLE_RANGE             11
#define ARRAY_JUMP_SIZE                0.01
#define ADC_BAC_DIVISOR                100.0

//-----------------------------------------------------------------------------
// Buzzer defines
//-----------------------------------------------------------------------------
#define LOWBUZZ                        880
#define HIGHBUZZ                       1175
#define BUZZ_LOW_FREQ                  0x27
#define BUZZ_HIGH_FREQ                 0x17

//-----------------------------------------------------------------------------
//Mode selection defines
//-----------------------------------------------------------------------------
#define DIP_SWITCH_MASK                0x3F
#define BAC_PERCENT_CHOSEN_RL          0x00
#define RAW_ADC_COUNT_GAS_CHOSEN_RL    0x01
#define RAW_ADC_COUNT_GAS_RL1          0x21
#define RAW_ADC_COUNT_GAS_RL2          0x11
#define RAW_ADC_COUNT_GAS_RL3          0x31
#define RAW_ADC_COUNT_GAS_RL4          0x09
#define RAW_ADC_COUNT_GAS_RL5          0x29
#define TEMP_IN_F                      0x03
#define TEMP_IN_C                      0x23
#define RAW_ADC_COUNT_TEMP             0x13
#define LCD_TEST_MODE                  0x07

//-----------------------------------------------------------------------------
//Resistance Defines
//-----------------------------------------------------------------------------
#define RL1       1
#define RL2       5
#define RL3       10
#define RL4       20
#define RL5       50
#define RLCHOSEN  10

//-----------------------------------------------------------------------------
//Function declarations
//-----------------------------------------------------------------------------
static void Initialization(void);
static void displayBAC(char * buffer, char * lastString, unsigned int * tHist,
                       unsigned int * gHist);
static void LCDTestMode(void);
static void MainDelay(unsigned int delayAmount);
static unsigned int tempF(unsigned int * tHist);
static unsigned int tempC(unsigned int * tHist);
static unsigned int readGasWithR(int resistance, unsigned int * gasHist);
static float adcTObac(int adc);
static float tempAdjust(float bac, unsigned int * tHist);
static void lowFreqBuzz(char * lastString);
static void highFreqBuzz(char * lastString);
static void displayUpdate(char * buffer, char * lastString);

const unsigned int BAC_ADC_COUNTS [] =
{
   831, 844, 856, 868, 880, 891, 901, 912, 922, 931, 940
};

int main()
{
   Initialization();
   char buff[LINE_LENGTH];
   char last[LINE_LENGTH];
   last[0] = 0;
   unsigned int tempHist = getAnalogData(TEMP_CHANNEL);
   unsigned int gasHist = getAnalogData(GAS_CHANNEL);
   unsigned int i;
   clearDisplay();
   while(1)
   {
      int portBReadout = (PORTB & DIP_SWITCH_MASK);
      if (portBReadout == BAC_PERCENT_CHOSEN_RL)
         displayBAC(&buff, &last, &tempHist, &gasHist);
      else if(portBReadout == RAW_ADC_COUNT_GAS_CHOSEN_RL)
         sprintf(buff, "CHN RL GAS: %u", readGasWithR(RLCHOSEN, &gasHist));
      else if(portBReadout == RAW_ADC_COUNT_GAS_RL1)
         sprintf(buff, "RL1 GAS: %u", readGasWithR(RL1, &gasHist));
      else if(portBReadout == RAW_ADC_COUNT_GAS_RL2)
         sprintf(buff, "RL2 GAS: %u", readGasWithR(RL2, &gasHist));
      else if(portBReadout == RAW_ADC_COUNT_GAS_RL3)
         sprintf(buff, "RL3 GAS: %u", readGasWithR(RL3, &gasHist));
      else if(portBReadout == RAW_ADC_COUNT_GAS_RL4)
         sprintf(buff, "RL4 GAS: %u", readGasWithR(RL4, &gasHist));
      else if(portBReadout == RAW_ADC_COUNT_GAS_RL5)
         sprintf(buff, "RL5 GAS: %u", readGasWithR(RL5, &gasHist));
      else if(portBReadout == TEMP_IN_F)
         sprintf(buff, "%u Fahrenheit", tempF(&tempHist));
      else if(portBReadout == TEMP_IN_C)
         sprintf(buff, "%u Celsius", tempC(&tempHist));
      else if(portBReadout == RAW_ADC_COUNT_TEMP)
      {
         smoothed(TEMP_CHANNEL, TEMP_ALPHA, &tempHist);
         sprintf(buff, "RAW TEMP: %u", tempHist);
      }
      else if(portBReadout == LCD_TEST_MODE)
      {
         LCDTestMode();
         strcpy(last, "");
      }
      else if(portBReadout == BUZZ_LOW_FREQ)
         lowFreqBuzz(&last);
      else if(portBReadout == BUZZ_HIGH_FREQ)
         highFreqBuzz(&last);
      displayUpdate(&buff, &last);
      MainDelay(FLASH_TIME);
   }
}

// -------------------------------------------------------------------
// Initializes Configuration Registers
// -------------------------------------------------------------------
static void Initialization(void)
{
   ADCON1 = ADCON1_MASK;
   INTCON = INTCON_MASK;
   OPTION_REG = OPTION_REG_MASK;
   TRISA = TRISA_MASK;
   TRISB = TRISB_MASK;
   TRISC = TRISC_MASK;
   TRISD = TRISD_MASK;
   SSPSTAT = SSPSTAT_MASK;
   SSPCON = SSPCON_MASK;
   T2CON = T2CON_MASK;
   CCP1CON = CCP1CON_MASK;
   initLCD();
}

// -------------------------------------------------------------------
// Display BAC percentage, and sets off the buzzer at a low frequency
// if the BAC is over .060 and at a high frequency if the BAC is over
// .075.
// -------------------------------------------------------------------
static void displayBAC(char * buffer, char * lastString, unsigned int * tHist,
                       unsigned int * gHist)
{
   int tempReadout = (PORTB & DIP_SWITCH_MASK);
   while(tempReadout == BAC_PERCENT_CHOSEN_RL)
   {
      int adcVal = readGasWithR(RLCHOSEN, gHist);
      float bac = adcTObac(adcVal);
      float temp = tempAdjust(bac, tHist);
      unsigned int val = (int)(bac * TEMP_MULT);

      if(val != MAX_BAC_VAL)
         sprintf(buffer, "BAC: 0.0%u", val);
      else
         sprintf(buffer, "BAC: 0.1");

      if(val >= CAUTION && val < DONT_DRIVE)
      {
         initPWM(LOWBUZZ);
         pwmON();
      }
      else if(val >= DONT_DRIVE )
      {
         initPWM(HIGHBUZZ);
         pwmON();
      }
      else
         pwmOFF();
      tempReadout = (PORTB & DIP_SWITCH_MASK);
      displayUpdate(buffer, lastString);
      MainDelay(FLASH_TIME);
   }
   pwmOFF();
}

// -------------------------------------------------------------------
// Converts the passed ADC reading into a BAC value
// -------------------------------------------------------------------
static float adcTObac(int adc)
{
   int i;
   for(i = 0; i < LOOKUP_TABLE_RANGE; i++)
      if(adc <= BAC_ADC_COUNTS[i])
      {
         if(i == 0)
            return 0;
         else
         {
            float range = BAC_ADC_COUNTS[i] - BAC_ADC_COUNTS[i - 1];
            int lowlim = BAC_ADC_COUNTS[i-1];
            return ARRAY_JUMP_SIZE * (adc - lowlim) / range + (i - 1) / ADC_BAC_DIVISOR;
         }
      }
   return .1;
}

// -------------------------------------------------------------------
// Adjusts BAC for room temperature
// -------------------------------------------------------------------
static float tempAdjust(float bac, unsigned int * tHist)
{
   float tempAdj = TEMP_ADJUST_SLOPE * tempC(tHist) + TEMP_ADJUST_INTERCEPT ;
   return bac * tempAdj;
}

// -------------------------------------------------------------------
// Reads the smoothed gas sensor reading with passed resistance
// -------------------------------------------------------------------
static unsigned int readGasWithR(int resistance, unsigned int * gasHist)
{
   *gasHist = getAnalogData(GAS_CHANNEL);
   setResistance(resistance);
   smoothed(GAS_CHANNEL, GAS_ALPHA, gasHist);
   return *gasHist;
}

// -------------------------------------------------------------------
// Reads the current smoothed temperature from the sensor and 
// returns it in degrees Celsius
// -------------------------------------------------------------------
static unsigned int tempC(unsigned int * tHist)
{
   smoothed(TEMP_CHANNEL, TEMP_ALPHA, tHist);
   return *tHist * CELSIUS_VAL / ADC_RES;
}

// -------------------------------------------------------------------
// Reads the current smoothed temperature from the sensor and 
// returns it in degrees Fahrenheit
// -------------------------------------------------------------------
static unsigned int tempF(unsigned int * tHist)
{
   unsigned int val = (unsigned int)(tempC(tHist) * CTOF_MULT + CTOF_CONST);
   return val;
}

//-----------------------------------------------------------------------------
//Displays a counter that increments every quarter second.
//-----------------------------------------------------------------------------
static void LCDTestMode(void)
{
   unsigned int count = 0;
   char countString[LINE_LENGTH];
   int portBReadout = (PORTB & DIP_SWITCH_MASK);
   while(portBReadout == LCD_TEST_MODE)
   {
      sprintf(countString, "%d", count);
      sendString(countString);
      MainDelay(QUARTER_SEC_DELAY);
      clearDisplay();
      count++;
      portBReadout = (PORTB & DIP_SWITCH_MASK);
   }   
   
}   

// -------------------------------------------------------------------
// Sets the buzzer to beep at a low frequency buzz.
// -------------------------------------------------------------------
static void lowFreqBuzz(char * lastString)
{
   //Buzzer at low frequency, display frequency on LCD
   clearDisplay();
   sendString("880 Hz");
   initPWM(LOWBUZZ);
   pwmON();
   int tempReadout = (PORTB & DIP_SWITCH_MASK);
   while(tempReadout == BUZZ_LOW_FREQ)
      tempReadout = (PORTB & DIP_SWITCH_MASK);
   pwmOFF();
   strcpy(lastString, "");
}

// -------------------------------------------------------------------
// Sets the buzzer to beep at a high frequency buzz.
// -------------------------------------------------------------------
static void highFreqBuzz(char * lastString)
{
   //Buzzer at High frequency, display frequency on LCD
   clearDisplay();
   sendString("1175 Hz");
   initPWM(HIGHBUZZ);
   pwmON();
   int tempReadout = (PORTB & DIP_SWITCH_MASK);
   while(tempReadout == BUZZ_HIGH_FREQ)
      tempReadout = (PORTB & DIP_SWITCH_MASK);
   pwmOFF();
   strcpy(lastString, "");
}

// -------------------------------------------------------------------
// Updates the display if buffer has changed, else does nothing.
// -------------------------------------------------------------------
static void displayUpdate(char * buffer, char * lastString)
{
   if(strcmp(buffer, lastString) != 0)
   {
      clearDisplay();
      strcpy(lastString, buffer);
      sendString(buffer);
   }
}

// -------------------------------------------------------------------
// Delay used in the main
// -------------------------------------------------------------------
static void MainDelay(unsigned int delayAmount)
{
   int i;
   for ( i = 0; i < delayAmount; i++)
      asm("nop");
}   

#endif