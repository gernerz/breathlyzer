//---------------------------------------------------------------------
// Group:   ger_mar_sch_spe
// Project: SE4130 Lab5: Blood Alcohol Content Monitor
// Purpose: This file implements the methods declared in PWM.h.
//          These methods drive the pulse width modulator, either setting
//          it up (initializing) or setting the tone the device will produce.
//---------------------------------------------------------------------
#include "PWM.h"

#define DUTY_DIVISOR       1
#define LOW_DUTY_PIN       4
#define TMR2_EN_PIN        2
#define PERIOD_VAL         62500
#define T2CON_CONF         0x07
#define PWM_ON             0x0C

//####################################################################
//### Private methods                                                #
//####################################################################
// -------------------------------------------------------------------
// Sets the duty cycle to period / 2^divPow2. For example, to set the duty
// cycle to 25% of the peroid divPow2 should equal 2 because 2^2 = 4 therefore
// period / 4 = 25% of the peroid.
// -------------------------------------------------------------------
static void setDutyCyc(BYTE period, BYTE divPow2)
{
   CCP1CON &= ~(3 << LOW_DUTY_PIN); // Clear pin 4-5
   CCPR1L = period >> divPow2;
}

// -------------------------------------------------------------------
// Sets the pwm period to the passed value
// -------------------------------------------------------------------
static void setPeriod(BYTE period)
{
   PR2 = period;
   //TMR2 = period;
}

//####################################################################
//### Public methods                                                 #
//####################################################################
// -------------------------------------------------------------------
// Initializes PWM unit
// -------------------------------------------------------------------
void initPWM(int freq)
{
   setFreq(freq);
   TRISC2 = 0;
   //This will set pre-scaler to 16 and enable
   T2CON |= T2CON_CONF;  //(1 << TMR2_EN_PIN); 
}

// -------------------------------------------------------------------
// Given a frequency in Hz it sets the period and the duty cycle of 
// the PWM appropriatly.
// -------------------------------------------------------------------
void setFreq(int freq)
{
   int period = PERIOD_VAL / freq - 1;
   setPeriod(period);
   setDutyCyc(period, DUTY_DIVISOR);
}

// -------------------------------------------------------------------
// Enables PWM output
// -------------------------------------------------------------------
void pwmON(void)
{
   CCP1CON = PWM_ON;
}

// -------------------------------------------------------------------
// Disables PWM output
// -------------------------------------------------------------------
void pwmOFF(void)
{
   CCP1CON = 0x00;
}

//####################################################################
//### Tests                                                          #
//####################################################################
#ifdef __TESTING_PWM
void main(void)
{
    initPWM(1500);
    setFreq(880);
    pwmON();
}
#endif