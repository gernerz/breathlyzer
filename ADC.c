//-----------------------------------------------------------------------------
// Group:   ger_mar_sch_spe
// Project: SE4130 Lab5: Blood Alcohol Content Monitor
// Purpose: This file implements the methods from ADC.h. This module supports 
//          reading and smoothing data from a PIC16 Analog to Digital Converter.
//-----------------------------------------------------------------------------
#include "ADC.h"

#define ADRSH_MASK   3
#define ADRESL_WIDTH 8
#define CH_OFFSET    3
#define ADC_CONF     0x41
#define DELAY_VAL    3

//####################################################################
//### Public methods                                                 #
//####################################################################
// -------------------------------------------------------------------
// Returns the value read from the passed ADC channel
// -------------------------------------------------------------------
unsigned int getAnalogData(BYTE analogChannel)
{
   // Use bit manipulations to set ADCON0 in a single assignment statement
   ADCON0 = ADC_CONF | (analogChannel << CH_OFFSET);
   // Busy-wait loop for the Acquisition time, about 25-30 µs
   long i;
   for(i=0; i < DELAY_VAL; i++);//DO NOTHING 
   // Set GO_nDONE = 1 to start the conversion
   GO_nDONE = 1;
   // Loop until GO_nDONE goes back to 0
   while(GO_nDONE);//DO NOTHING
   // Return the result as a single return statement (use bit manipulations)
   return (((unsigned int)ADRESH & ADRSH_MASK) << ADRESL_WIDTH) | ADRESL;
}

// -------------------------------------------------------------------
// Reads a value from the passed ADC channel, applies smoothing with
// the passed alpha, and stores its new result in the passed history variable
// -------------------------------------------------------------------
smoothed(unsigned char ch, float smoothRat, unsigned int * history)
{
   unsigned int data = getAnalogData(ch);
   *history = (unsigned int)((smoothRat * *history) + ((1 - smoothRat) * data));
}

//####################################################################
//### Tests                                                          #
//####################################################################
#ifdef __TESTING_ADC

#include "LCD.h"
#include "SPI.h"
#include <string.h>
#include <stdio.h>

unsigned int toC(unsigned int val)
{
   return val*500/1024;

}

unsigned int toF(unsigned int val)
{
   return (int)(toC(val) * 9 / 5.0 + 32);
}

void main(void)
{
   //LCD STUFF
   TRISD = 0x00;
   TRISC &= 0x17;  // B'0001 0111'
   initLCD();
   SSPSTAT = 0x40; // B'0100 0000'
   SSPCON = 0x30;  // B'0011 0000'
   ADCON1 = 0x84;

   char buff[16];
   buff[0];

   clearDisplay();
   unsigned int history = getAnalogData(0);
   unsigned int tmp;
   while(1)
   {
      tmp = toF(getAnalogData(0));
      sprintf(buff, "%dF", tmp);
      clearDisplay();
      sendString(buff);
      for(int i=0; i<0x3FFF; i++) asm("nop");
   }
}

#endif