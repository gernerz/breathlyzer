//---------------------------------------------------------------------
// Group:   ger_mar_sch_spe
// Project: SE4130 Lab5: Blood Alcohol Content Monitor
// Purpose: This class supports reading and smoothing data from a PIC16
//          Analog to Digital Converter.
//---------------------------------------------------------------------
#ifndef __ADC_H
#define __ADC_H
#include <pic.h>
#include "Globals.h"

// -------------------------------------------------------------------
// Returns the value read from the passed ADC channel
// -------------------------------------------------------------------
unsigned int getAnalogData(BYTE analogChannel);

// -------------------------------------------------------------------
// Reads a value from the passed ADC channel, applies smoothing with
// the passed alpha, and stores its new result in the passed history variable
// -------------------------------------------------------------------
smoothed(unsigned char ch, float smoothRat, unsigned int * history);

#endif