//---------------------------------------------------------------------
// Group:   ger_mar_sch_spe
// Project: SE4130 Lab5: Blood Alcohol Content Monitor
// Purpose: This file implements the methods declared in PWM.h.
//          These methods drive the pulse width modulator, either setting
//          it up (initializing) or setting the tone the device will produce.
//---------------------------------------------------------------------
#ifndef __PWM_H
#define __PWM_H
#include <pic.h>
#include "Globals.h"

// -------------------------------------------------------------------
// Initializes PWM unit
// -------------------------------------------------------------------
void initPWM(int freq);

// -------------------------------------------------------------------
// Given a frequency in Hz it sets the period and the duty cycle of 
// the PWM appropriatly.
// -------------------------------------------------------------------
void setFreq(int freq);

// -------------------------------------------------------------------
// Enables PWM output
// -------------------------------------------------------------------
void pwmON(void);

// -------------------------------------------------------------------
// Disables PWM output
// -------------------------------------------------------------------
void pwmOFF(void);

#endif