 //---------------------------------------------------------------------
// Group:   ger_mar_sch_spe
// Project: SE4130 Lab5: Blood Alcohol Content Monitor
// Purpose: This file implements the methods in SPI.h. This module is 
//           designed to drive the SPI interface on a PIC16. Specialized 
//           methods are included to drive a digital potentiometer. 
//---------------------------------------------------------------------
#include "SPI.h"

#define SSBUF_CONF   0x13
#define SLAVE_SELECT RA3
#define STEP_POW_TWO 1
#define STEP         5 // floor(255 / MaxRes * 2 ^ STEP_POW_TWO) 
#define CHIP_SELECT  RD1

//####################################################################
//### Public methods                                                 #
//####################################################################
// -------------------------------------------------------------------
// Send and recieve data accross the SPI interface
// -------------------------------------------------------------------
void sendSPIData(BYTE buffer)
{
   //BYTE temp = buffer;
   SLAVE_SELECT = 0;
   CHIP_SELECT = 0;
   SSPBUF = SSBUF_CONF;
   while(!BF)
      ;/*Do Nothing*/
   SSPBUF = buffer;
   while(!BF)
      ;/*Do Nothing*/
   CHIP_SELECT = 1;
   BYTE unusedByte = SSPBUF;
   SLAVE_SELECT = 1;
}

// -------------------------------------------------------------------
// Sets the resistance to the passed value. Output range: [0.1k,99k]
// -------------------------------------------------------------------
void setResistance(int res)
{
   BYTE val = (STEP * res) >> STEP_POW_TWO;
   sendSPIData(val);
}

//####################################################################
//### Tests                                                          #
//####################################################################
#ifdef __TESTING_SPI
void main(void)
{
   TRISD = 0x00;
   TRISC &= 0xD7;  // B'1101 0111'
   SSPSTAT = 0x40; // B'0100 0000'
   SSPCON = 0x30;  // B'0011 0000'

   BYTE b;
   b = 0x00;
   setResistance(50);
   while(1);
}
#endif